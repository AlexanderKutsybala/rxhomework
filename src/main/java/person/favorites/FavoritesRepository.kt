package person.favorites


import person.types.PersonWithAddress
import io.reactivex.Observable


internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {

        return personBackend.loadAllPersons()
                .concatMap {
                    val personList = it
                    favoritesDatabase.favoriteContacts()
                            .concatMap {
                                var newPersonList = listOf<PersonWithAddress>()
                                for (personWithAddress in personList) {
                                    for (id in it) {
                                        if (id==personWithAddress.person.id) {
                                            newPersonList += personWithAddress
                                        }
                                    }
                                }
                                Observable.fromArray(newPersonList)
                            }
                }
    }
}


