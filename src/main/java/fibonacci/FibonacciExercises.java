package fibonacci;

import java.util.HashMap;

import io.reactivex.Observable;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {

	    return Observable.range(0, n).scan(new HashMap<String, Integer>(), (m, k) -> {
			Integer f1 = m.get("f1");
			Integer f2 = m.get("f2");
			f1 = f1 == null ? 0 : f1;
			f2 = f2 == null ? 1 : f2;
			int fn = f1 + f2;
			m.put("f1", f2);
			m.put("f2", fn);
			return m;
		})
				.filter(m -> m.size() == 2)
			    .map(m -> m.get("f1") + m.get("f2"))
				.startWith(Observable.just(0, 1, 1))
				.take(n);



	}

}
